import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainController implements Initializable{

	
   // private final SQLConnectionClass databaseConnection = new SQLConnectionClass();
   // private final ObservableList<KundenSimpleStringClass> kundeObservableList = FXCollections.observableArrayList();
    
    private final ObservableList<RechnungSimpleStringClass> rechnungObservableList = FXCollections.observableArrayList();
    
    private final ObservableList<LeistungenSimpleStringClass> leistungObservableList = FXCollections.observableArrayList();
    
    private static int kundenID = -1;
    
    private static Stage stage;
	
	   @FXML
	    private MenuItem Beenden;

	    @FXML
	    private MenuItem Exportieren;

	    @FXML
	    private MenuItem HinzufuegenRechnung;

	    @FXML
	    private MenuItem KundeHinzufuegen;

	    @FXML
	    private Button LeistungHinzufuegen;

	    @FXML
	    private Button RechnungHinzufuegen;

	    @FXML
	    private MenuItem UnternehmenHinzufuegen;

	    @FXML
	    private TableColumn<?, ?> colOrtKunde;

	    @FXML
	    private TextField bezeichnung;

	    @FXML
	    private TableColumn<?, ?> colAnredeKunde;

	    @FXML
	    private TableColumn<?, ?> colBezeichnungKunde;

	    @FXML
	    private TableColumn<?, ?> colBezeichnungLeistung;

	    @FXML
	    private TableColumn<?, ?> colBruttoRechnung;

	    @FXML
	    private TableColumn<?, ?> colErstelldatumRechnung;

	    @FXML
	    private TableColumn<?, ?> colHausnummerKunde;

	    @FXML
	    private TableColumn<?, ?> colIDLeistung;

	    @FXML
	    private TableColumn<?, ?> colKontaktRechnung;

	    @FXML
	    private TableColumn<?, ?> colKontaktpersonKunde;

	    @FXML
	    private TableColumn<?, ?> colLandKunde;

	    @FXML
	    private TableColumn<?, ?> colMailKunde;

	    @FXML
	    private TableColumn<?, ?> colMengeLeistung;

	    @FXML
	    private TableColumn<?, ?> colNettoRechnung;

	    @FXML
	    private TableColumn<?, ?> colPLZKunde;

	    @FXML
	    private TableColumn<?, ?> colPreisLeistung;

	    @FXML
	    private TableColumn<?, ?> colRechnungBeglRechnung;

	    @FXML
	    private TableColumn<?, ?> colRechnungsdatumRechnung;

	    @FXML
	    private TableColumn<?, ?> colRechnungsnummerRechnung;

	    @FXML
	    private TableColumn<?, ?> colStrasseKunde;

	    @FXML
	    private TableColumn<?, ?> colTelefonnummerKunde;

	    @FXML
	    private TableColumn<?, ?> colUIDKunde;

	    @FXML
	    private TextField kontaktperson;

	    @FXML
	    private TextField ort;

	    @FXML
	    private TextField plz;

	    @FXML
	    private TextField rechnungsnummer;

	    @FXML
	    private TextField strasse;

	   // @FXML
	   // private TableView<KundenSimpleStringClass> tableViewKunde;
	    
	    @FXML
	    private TableView<RechnungSimpleStringClass> tableViewRechnung;
	    
	    @FXML
	    private TableView<LeistungenSimpleStringClass> tableViewLeistungen;

	    @FXML
	    private MenuItem ueber;

	    @FXML
	    private TextField uid;
	    
	    
    
    
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

//        fillTableKunde();
//        fillTableRechnung();
//        fillTableLeistung();
        
    }
    
    public int getKundenID() {
		return kundenID;
	}  

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public static void setKundenID(int kundenID) {
		MainController.kundenID = kundenID;
	}

	@FXML
    void Exportieren(ActionEvent event) {

    }
    

    @FXML
    void Beenden(ActionEvent event) 
    {
    	Platform.exit();
    }

    @FXML
    void RechnungHinzufuegen(ActionEvent event) throws IOException 
    {


    	Parent fxmlLoader =  FXMLLoader.load((getClass().getResource("Rechnung.fxml")));
     
        Stage stage = new Stage();
    	stage.setResizable(false);
        stage.setScene(new Scene(fxmlLoader));
        stage.show();
    	
    }

    @FXML
    void KundeHinzufuegen(ActionEvent event) throws IOException 
    {

    	Parent fxmlLoader =  FXMLLoader.load((getClass().getResource("AddKunde.fxml")));

    	stage = new Stage();
    	stage.setResizable(false);
    	stage.setScene(new Scene(fxmlLoader));
    	stage.show();

    }

    @FXML
    void LeistungHinzufuegen(ActionEvent event) throws IOException 
    {


    	Parent fxmlLoader =  FXMLLoader.load((getClass().getResource("Leistung.fxml")));
        
    	Stage stage = new Stage();
    	stage.setResizable(false);
    	stage.setScene(new Scene(fxmlLoader));
        stage.show();
    	
    }
  
    @FXML
    void UnternehmenHinzufuegen(ActionEvent event) throws IOException 
    {

    	Parent fxmlLoader =  FXMLLoader.load((getClass().getResource("AddKunde.fxml")));

        stage = new Stage(); 
    	stage.setResizable(false);
        stage.setScene(new Scene(fxmlLoader));
        stage.show();
    	
    }
    
    @FXML
    void KundeBearbeiten(ActionEvent event) throws IOException {
    	
    	
    	//kundenID = Integer.parseInt(tableViewKunde.getSelectionModel().getSelectedItem().getID()) - 1;
    	
    	Parent fxmlLoader =  FXMLLoader.load((getClass().getResource("AddKunde.fxml")));

        stage = new Stage(); 
    	stage.setResizable(false);
        stage.setScene(new Scene(fxmlLoader));
        stage.show();
    }
    
    @FXML
    void Ueber(ActionEvent event) 
    {
    	Alert a = new Alert(AlertType.NONE);
    	a.setAlertType(AlertType.INFORMATION);
    	a.setContentText("�BER UNS");
    	a.show();

    }

	
//    public void fillTableKunde() {
//        try {
//        	System.out.println("lol");
//            readKunde();
//        } catch (SQLException e) {
//            circleVerbindung.setFill(Color.RED);
//            labConnection.setText("Datenbankverbindung: 'ERROR'");
//        }
//
//        colBezeichnungKunde.setCellValueFactory(new PropertyValueFactory<>("Bezeichnung"));
//        colAnredeKunde.setCellValueFactory(new PropertyValueFactory<>("Anrede"));
//        colKontaktpersonKunde.setCellValueFactory(new PropertyValueFactory<>("Kontaktperson"));
//        colStrasseKunde.setCellValueFactory(new PropertyValueFactory<>("Strasse"));
//        colHausnummerKunde.setCellValueFactory(new PropertyValueFactory<>("Hausnummer"));
//        colPLZKunde.setCellValueFactory(new PropertyValueFactory<>("PLZ"));
//        colLandKunde.setCellValueFactory(new PropertyValueFactory<>("Land"));
//        colOrtKunde.setCellValueFactory(new PropertyValueFactory<>("Ort"));
//        colUIDKunde.setCellValueFactory(new PropertyValueFactory<>("UID"));
//        colMailKunde.setCellValueFactory(new PropertyValueFactory<>("Email"));
//        colTelefonnummerKunde.setCellValueFactory(new PropertyValueFactory<>("Telefon"));
//
//       // tableViewKunde.setItems(kundeObservableList);
//    }
//    
//    	public void fillTableRechnung() {
//    		try {
//    			readRechnung();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    		
//        colKontaktRechnung.setCellValueFactory(new PropertyValueFactory<>("Kontaktperson"));
//        colErstelldatumRechnung.setCellValueFactory(new PropertyValueFactory<>("Erstelldatum"));
//        colBruttoRechnung.setCellValueFactory(new PropertyValueFactory<>("Brutto"));
//        colNettoRechnung.setCellValueFactory(new PropertyValueFactory<>("Netto"));
//        colRechnungsnummerRechnung.setCellValueFactory(new PropertyValueFactory<>("Rechnungsnummer"));
//        colRechnungsdatumRechnung.setCellValueFactory(new PropertyValueFactory<>("Rechnungsdatum"));
//        colRechnungBeglRechnung.setCellValueFactory(new PropertyValueFactory<>("RechnungBeglichen"));
//
//        tableViewRechnung.setItems(rechnungObservableList);
//    }
//    
//    public void fillTableLeistung() {
//        try {
//        	readLeistung();
//        
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        colIDLeistung.setCellValueFactory(new PropertyValueFactory<>("ID"));
//        colBezeichnungLeistung.setCellValueFactory(new PropertyValueFactory<>("Bezeichnung"));
//        colMengeLeistung.setCellValueFactory(new PropertyValueFactory<>("Menge"));
//        colPreisLeistung.setCellValueFactory(new PropertyValueFactory<>("Preis"));
//
//        tableViewLeistungen.setItems(leistungObservableList);
//    }
//    
// //   public ObservableList<KundenSimpleStringClass> readKunde() throws SQLException {
//////        databaseConnection.connect();
//////
//////        Statement statement = databaseConnection.getConnection().createStatement();
//////        ResultSet resultSet = statement.executeQuery("SELECT * FROM Kunden");
//////
//////        while (resultSet.next()) {
//////            KundenSimpleStringClass kunde = new KundenSimpleStringClass(
//////            		resultSet.getString("ID"),
//////                    resultSet.getString("Bezeichnung"),
//////                    resultSet.getString("Anrede"),
//////                    resultSet.getString("Kontaktperson"),
//////                    resultSet.getString("Strasse"),
//////                    resultSet.getString("Hausnummer"),
//////                    resultSet.getString("PLZ"),
//////                    resultSet.getString("Land"),
//////                    resultSet.getString("Ort"),
//////                    resultSet.getString("UID"),
//////                    resultSet.getString("Email"),
//////                    resultSet.getString("Telefon")
//////            );
//////            kundeObservableList.add(kunde);
//////        }
//////        databaseConnection.getConnection().close();
//////		return kundeObservableList;
//////    }
////    
//   public void readRechnung() throws SQLException{
//        databaseConnection.connect();
//
//        Statement statement = databaseConnection.getConnection().createStatement();
//        ResultSet resultSet = statement.executeQuery("SELECT * FROM Rechnungen");
//
//        while (resultSet.next()) {
//
//           RechnungSimpleStringClass rechnung = new RechnungSimpleStringClass(
//                    resultSet.getString("Kontaktperson"),
//                    resultSet.getString("Erstelldatum"),
//                    resultSet.getString("Brutto"),
//                    resultSet.getString("Netto"),
//                    resultSet.getString("Rechnungsnummer"),
//                    resultSet.getString("Rechnungsdatum"),
//                    resultSet.getString("RechnungBeglichen")
//           );
//
//            rechnungObservableList.add(rechnung);
//        }
//        databaseConnection.getConnection().close();
//    }
//    
//    public ObservableList<LeistungenSimpleStringClass> readLeistung() throws SQLException{
//        databaseConnection.connect();
//
//        Statement statement = databaseConnection.getConnection().createStatement();
//        ResultSet resultSet = statement.executeQuery("SELECT * FROM Leistungen");
//
//        while (resultSet.next()) {
//            LeistungenSimpleStringClass leistung = new LeistungenSimpleStringClass(
//                    resultSet.getString("ID"),
//                    resultSet.getString("Bezeichnung"),
//                    resultSet.getString("Menge"),
//                    resultSet.getString("Preis")
//            );
//
//            leistungObservableList.add(leistung);
//        }
//        databaseConnection.getConnection().close();
//        return leistungObservableList;
//    }
//    



		private void readLeistung() {
		// TODO Auto-generated method stub
		
	}

		private void readRechnung() {
			// TODO Auto-generated method stub
			
		}
}



	
	
	



