import javafx.beans.property.SimpleStringProperty;

public class RechnungSimpleStringClass {
	    private final SimpleStringProperty Kontaktperson;
	    private final SimpleStringProperty Erstelldatum;
	    private final SimpleStringProperty Brutto;
	    private final SimpleStringProperty Netto;
	    private final SimpleStringProperty Rechnungsnummer;
	    private final SimpleStringProperty Rechnungsdatum;
	    private final SimpleStringProperty RechnungBeglichen;

	    public RechnungSimpleStringClass() {
	        this.Kontaktperson = null;
	        this.Erstelldatum = null;
	        this.Brutto = null;
	        this.Netto = null;
	        this.Rechnungsnummer = null;
	        this.Rechnungsdatum = null;
	        this.RechnungBeglichen = null;
	    }
	    
	    public RechnungSimpleStringClass(String kontaktperson, String erstelldatum, String brutto, String netto, String rechnungsnummer, String rechnungsdatum, String RechnungBeglichen) {
	        this.Kontaktperson = new SimpleStringProperty(kontaktperson);
	        this.Erstelldatum = new SimpleStringProperty(erstelldatum);
	        this.Brutto = new SimpleStringProperty(brutto);
	        this.Netto = new SimpleStringProperty(netto);
	        this.Rechnungsnummer = new SimpleStringProperty(rechnungsnummer);
	        this.Rechnungsdatum = new SimpleStringProperty(rechnungsdatum);
	        this.RechnungBeglichen = new SimpleStringProperty(RechnungBeglichen);
	    }
	    

	    public String getKontaktperson() {
	        assert Kontaktperson != null;
	        return Kontaktperson.get();
	    }

	    public void setKontaktperson(String kontaktperson) {
	        assert this.Kontaktperson != null;
	        this.Kontaktperson.set(kontaktperson);
	    }

	    public String getErstelldatum() {
	        assert Erstelldatum != null;
	        return Erstelldatum.get();
	    }

	    public void setErstelldatum(String erstelldatum) {
	        assert this.Erstelldatum != null;
	        this.Erstelldatum.set(erstelldatum);
	    }

	    public String getBrutto() {
	        assert Brutto != null;
	        return Brutto.get();
	    }

	    public void setBrutto(String brutto) {
	        assert this.Brutto != null;
	        this.Brutto.set(brutto);
	    }

	    public String getNetto() {
	        assert Netto != null;
	        return Netto.get();
	    }

	    public void setNetto(String netto) {
	        assert this.Netto != null;
	        this.Netto.set(netto);
	    }

	    public String getRechnungsnummer() {
	        assert Rechnungsnummer != null;
	        return Rechnungsnummer.get();
	    }

	    public void setRechnungsnummer(String rechnungsnummer) {
	        assert this.Rechnungsnummer != null;
	        this.Rechnungsnummer.set(rechnungsnummer);
	    }

	    public String getRechnungsdatum() {
	        assert Rechnungsdatum != null;
	        return Rechnungsdatum.get();
	    }

	    public void setRechnungsdatum(String rechnungBeglichen) {
	        assert this.Rechnungsdatum != null;
	        this.Rechnungsdatum.set(rechnungBeglichen);
	    }
	    
	    public String getRechnungBeglichen() {
	        assert RechnungBeglichen != null;
	        return RechnungBeglichen.get();
	    }

	    public void setRechnungBeglichen(String rechnungsdatum) {
	        assert this.Rechnungsdatum != null;
	        this.Rechnungsdatum.set(rechnungsdatum);
	    }
}
