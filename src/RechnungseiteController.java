import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class RechnungseiteController implements Initializable{
	
	MainController maincontroller = new MainController();

    @FXML
    private Button btnAdd;

    @FXML
    private CheckBox cbBereitsBezahlt;

    @FXML
    private TableColumn<?, ?> colBezahlt;

    @FXML
    private TableColumn<?, ?> colBrutto;

    @FXML
    private TableColumn<?, ?> colID;

    @FXML
    private TableColumn<?, ?> colLeistung;

    @FXML
    private TableColumn<?, ?> colMenge;

    @FXML
    private TableColumn<?, ?> colNetto;

    @FXML
    private TableColumn<?, ?> colPreis;

    @FXML
    private TableColumn<?, ?> colSteuersatz;

    @FXML
    private TableColumn<?, ?> colUst;
    
    @FXML
    private TableColumn<?, ?> colBezeichnungLeistung;

    @FXML
    private DatePicker dpDatum;

    @FXML
    private TableView<LeistungenSimpleStringClass> tableViewLeistungen;

    @FXML
    private TableView<?> tableViewRechnung;

    @FXML
    private Text txtGesamtPreis;

    @FXML
    private TextField txtKundennummer;

    @FXML
    private Text txtRechnungsnummer;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
//		try {
//		fillTableLeistung();
//	} catch (SQLException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	}
    

    

    @FXML
    void AddLeistung(ActionEvent event) {
    }
   
    
//    public void fillTableLeistung() throws SQLException {
//    	ObservableList<LeistungenSimpleStringClass> leistungsBezeichnungListe = FXCollections.observableArrayList();
//    	
//    	for (LeistungenSimpleStringClass item : maincontroller.readLeistung()) {
//			leistungsBezeichnungListe.add(item);
//		}
//    	
//        colBezeichnungLeistung.setCellValueFactory(new PropertyValueFactory<>("Bezeichnung"));
//    	
//    	tableViewLeistungen.setItems(leistungsBezeichnungListe);
//    	

    	
 //   }




}
