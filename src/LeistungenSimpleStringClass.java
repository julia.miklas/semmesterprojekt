import javafx.beans.property.SimpleStringProperty;

public class LeistungenSimpleStringClass {
	public SimpleStringProperty ID;
	public SimpleStringProperty Bezeichnung;
	public SimpleStringProperty Menge;
	public SimpleStringProperty Preis;

	public LeistungenSimpleStringClass(String iD, String bezeichnung, String menge, String preis) {
		this.ID = new SimpleStringProperty(String.valueOf(iD));
		this.Bezeichnung = new SimpleStringProperty(String.valueOf(bezeichnung));
		this.Menge = new SimpleStringProperty(String.valueOf(menge));
		this.Preis = new SimpleStringProperty(String.valueOf(preis));
	}

	public final SimpleStringProperty IDProperty() {
		return this.ID;
	}

	public final String getID() {
		return this.IDProperty().get();
	}

	public final void setID(final String ID) {
		this.IDProperty().set(ID);
	}

	public final SimpleStringProperty BezeichnungProperty() {
		return this.Bezeichnung;
	}

	public final String getBezeichnung() {
		return this.BezeichnungProperty().get();
	}

	public final void setBezeichnung(final String Bezeichnung) {
		this.BezeichnungProperty().set(Bezeichnung);
	}

	public final SimpleStringProperty MengeProperty() {
		return this.Menge;
	}

	public final String getMenge() {
		return this.MengeProperty().get();
	}

	public final void setMenge(final String Menge) {
		this.MengeProperty().set(Menge);
	}

	public final SimpleStringProperty PreisProperty() {
		return this.Preis;
	}

	public final String getPreis() {
		return this.PreisProperty().get();
	}

	public final void setPreis(final String Preis) {
		this.PreisProperty().set(Preis);
	}
}
